# todo-glint


Url : http://todo-glint.herokuapp.com/api/v1/todo

post : http://todo-glint.herokuapp.com/api/v1/todo
send (json) : {
	"content":"Makan",
	"completed":false
}
response : {
    "success": true,
    "result": {
        "completed": false,
        "_id": "5d6406cb995fcc00177de086",
        "content": "Makan",
        "date": "2019-08-26T16:20:27.111Z",
        "__v": 0
    }
}

get : http://todo-glint.herokuapp.com/api/v1/todo
resposnse : {
"success": true,
"result": [
{
"completed": false,
"_id": "5d64048ab32ca720f746e89a",
"content": "Menari",
"date": "2019-08-26T16:10:50.397Z",
"__v": 0
},
{
"completed": false,
"_id": "5d64048fb32ca720f746e89b",
"content": "Manatap",
"date": "2019-08-26T16:10:55.313Z",
"__v": 0
},
{
"completed": false,
"_id": "5d64049eb32ca720f746e89c",
"content": "Merayap",
"date": "2019-08-26T16:11:10.112Z",
"__v": 0
},
{
"completed": false,
"_id": "5d6406cb995fcc00177de086",
"content": "Makan",
"date": "2019-08-26T16:20:27.111Z",
"__v": 0
}
]
}

put : http://todo-glint.herokuapp.com/api/v1/todo/:id
send (json) : {
	"completed":false
}
response : {
    "success": true,
    "result": {
        "completed": false
    }
}

delete : http://todo-glint.herokuapp.com/api/v1/todo/:id
response : {
    "success": true,
    "result": {
        "completed": false,
        "_id": "5d63ed128ec75714a9a5349c",
        "content": "Menari",
        "date": "2019-08-26T14:30:42.929Z",
        "__v": 0
    }
}




