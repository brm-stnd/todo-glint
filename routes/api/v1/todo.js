var express = require('express');
var router = express.Router();
var todoController = require('../../../controllers/api/v1/todoController.js');
const auth = require('../../../middleware/auth');

router.get('/', auth.isAuthenticated, todoController.getTodo)
    .post('/', auth.isAuthenticated, todoController.insertTodo);
router.get('/:id', auth.isAuthenticated, todoController.getTodoById)
    .put('/:id', auth.isAuthenticated, todoController.updateTodo)
    .delete('/:id', auth.isAuthenticated, todoController.deleteTodo);
router.get('/content/:content', auth.isAuthenticated, todoController.getTodoByContent);

module.exports = router;
