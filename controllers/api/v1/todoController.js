var FuncHelpers     = require('../../../helpers/response');
var Todos           = require('../../../models/api/v1/todo');

exports.getTodo = function(req, res, next){
    Todos.find().exec()
        .then((todo)=>{
            res.status(200).json(FuncHelpers.successResponse(todo));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.getTodoById = function(req, res, next){
    let id = req.params.id; 
    Todos.findById(id).exec()
        .then((todo)=>{
            res.status(200).json(FuncHelpers.successResponse(todo));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.getTodoByContent = function(req, res, next){
    let content = req.params.content;
    Todos.find({content: {$regex: content, $options: 'i'}})
        .then((todo)=>{
            res.status(200).json(FuncHelpers.successResponse(todo));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.insertTodo = function(req, res, next){
    Todos.create(req.body)
        .then((todo) => {
            res.status(201).json(FuncHelpers.successResponse(todo))
        })
        .catch((err) => {
            res.status(422).json(FuncHelpers.errorResponse(err))
        })
}

exports.updateTodo = function(req, res, next){  
    let id          = req.params.id; 
    let pengganti   = req.body;
    Todos.findOneAndUpdate({"_id":id}, pengganti).exec()
        .then((todo)=>{
            res.status(200).json(FuncHelpers.successResponse(pengganti));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}

exports.deleteTodo = function(req, res) {
    let id          = req.params.id; 
    Todos.findByIdAndRemove(id).exec()
        .then((todo)=>{
            res.status(200).json(FuncHelpers.successResponse(todo));
        })
        .catch((err)=>{
            res.status(422).json(FuncHelpers.errorResponse(err));
        });
}






