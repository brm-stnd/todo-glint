const mongoose      = require("mongoose");
const Schema        = mongoose.Schema;
var validate        = require('mongoose-validator')
var uniqueValidator = require('mongoose-unique-validator');

var contentValidator = [
    validate({
      validator: 'isLength',
      arguments: [4, 50],
      message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    })
  ]

const todoSchema = new Schema({
    content: {
        type: String,
        required: true,
        unique: true,
        validate: contentValidator
    },
    completed: {
        type: Boolean,
        default: false,
        required: true
    },
    date: { type: Date, default: Date.now },
}, { collection: 'todo' });

todoSchema.plugin(uniqueValidator); 

var Todo = mongoose.model("Todo", todoSchema);
module.exports = Todo;