const mongoose      = require("mongoose");
const Schema        = mongoose.Schema;
var validate        = require('mongoose-validator')
var uniqueValidator = require('mongoose-unique-validator');
const bcrypt        = require('bcryptjs');
const saltRounds    = 10;

var usernameValidator = [
    validate({
      validator: 'isLength',
      arguments: [6, 30],
      message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    })
  ]

const usersSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: usernameValidator
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    exp_token: { 
      type: Date
    },
    token: {
        type: String
    },
    is_verified: {
        type: Boolean,
        default: false,
        required: true
    },
    date: { 
        type: Date, 
        default: Date.now 
      }
}, { collection: 'users' });

usersSchema.pre('save', function(next) {
  var user = this
  bcrypt.hash(user.password, saltRounds, function(err, hash) {
    if(err) return next(err)
    user.password = hash
    next()
  })
});

usersSchema.pre('findOneAndUpdate', function(next) {
  var user = this
  if(user._update.password){
    bcrypt.hash(user._update.password, saltRounds, function(err, hash) {
      if(err) return next(err)
      user._update.password = hash
      next()
    })
  }else{
    next()
  }
});

usersSchema.plugin(uniqueValidator); 

var Users = mongoose.model("Users", usersSchema);
module.exports = Users;